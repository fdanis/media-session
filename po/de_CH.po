# German translation of pipewire
# Copyright (C) 2008 pipewire
# This file is distributed under the same license as the pipewire package.
#
# Micha Pietsch <barney@fedoraproject.org>, 2008, 2009.
# Fabian Affolter <fab@fedoraproject.org>, 2008-2009, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: pipewire\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/pipewire/pipewire-media-"
"session/issues/new\n"
"POT-Creation-Date: 2021-10-20 10:03+1000\n"
"PO-Revision-Date: 2012-01-30 09:53+0000\n"
"Last-Translator: Fabian Affolter <fab@fedoraproject.org>\n"
"Language-Team: German <fedora-trans-de@redhat.com>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Language: German\n"

#: src/alsa-monitor.c:662
msgid "Built-in Audio"
msgstr "Internes Audio"

#: src/alsa-monitor.c:666
msgid "Modem"
msgstr "Modem"

#: src/alsa-monitor.c:675
msgid "Unknown device"
msgstr ""
